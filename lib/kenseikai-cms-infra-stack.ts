import * as cdk from '@aws-cdk/core';
import * as cloudfront from "@aws-cdk/aws-cloudfront";
import * as s3 from "@aws-cdk/aws-s3";
import * as iam from "@aws-cdk/aws-iam";
import * as acm from "@aws-cdk/aws-certificatemanager";
import * as route53 from "@aws-cdk/aws-route53";
import * as route53Target from "@aws-cdk/aws-route53-targets";
import { assert } from "console";

export class KenseikaiCmsInfraStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    const oai = new cloudfront.OriginAccessIdentity(this, "knseikai-cf-oai", {
      comment: `kenseikai-cms`,
    });

    const webBucket = new s3.Bucket(this, "WebBucket", {
      bucketName: `kenseikai-web-${cdk.Stack.of(this).account}`,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      cors: [
        {
          allowedOrigins: ["archive.ut-shorinji.net"],
          allowedMethods: [s3.HttpMethods.HEAD, s3.HttpMethods.GET],
          allowedHeaders: ["*"],
        },
      ],
      websiteErrorDocument: "index.html",
      websiteIndexDocument: "index.html",
    });

    const webBucketPolicy = new iam.PolicyStatement({
      effect: iam.Effect.ALLOW,
      actions: ["s3:GetObject"],
      principals: [
        new iam.CanonicalUserPrincipal(
          oai.cloudFrontOriginAccessIdentityS3CanonicalUserId
        ),
      ],
      resources: [webBucket.bucketArn + "/*"],
    });
    webBucket.addToResourcePolicy(webBucketPolicy);


    const hostedZone = route53.PublicHostedZone.fromLookup(this, "HostedZone", {
      domainName: `archive.ut-shorinji.net`,
      privateZone: false,
    });

    const basicAuthFunction = new cloudfront.Function(
      this,
      "BasicAuthFunction",
      {
        functionName: `basic-authentication-frontend`,
        code: cloudfront.FunctionCode.fromFile({
          filePath: "lambda/basic-auth.js",
        }),
      }
    );

    const certificate = acm.Certificate.fromCertificateArn(this, 'Certificate', 'arn:aws:acm:us-east-1:069528432491:certificate/b8986db4-2ab5-4310-82b4-8f87ee87cc1c')

    const distribution = new cloudfront.CloudFrontWebDistribution(
      this,
      "MintoWebDistribution",
      {
        viewerCertificate: cloudfront.ViewerCertificate.fromAcmCertificate(
          certificate,
          {
            aliases: [`${hostedZone.zoneName}`],
            securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1,
            sslMethod: cloudfront.SSLMethod.SNI,
          }
        ),
        priceClass: cloudfront.PriceClass.PRICE_CLASS_ALL,
        originConfigs: [
          {
            s3OriginSource: {
              s3BucketSource: webBucket,
              originAccessIdentity: oai,
            },
            behaviors: [
              {
                isDefaultBehavior: true,
                minTtl: cdk.Duration.seconds(0),
                maxTtl: cdk.Duration.minutes(5),
                defaultTtl: cdk.Duration.minutes(5),
                pathPattern: "*",
                functionAssociations: [
                  {
                    eventType: cloudfront.FunctionEventType.VIEWER_REQUEST,
                    function: basicAuthFunction,
                  },
                ],
              },
            ],
          },
        ],
        errorConfigurations: [
          {
            errorCode: 403,
            responsePagePath: "/index.html",
            responseCode: 200,
            errorCachingMinTtl: 0,
          },
          {
            errorCode: 404,
            responsePagePath: "/index.html",
            responseCode: 200,
            errorCachingMinTtl: 0,
          },
        ],
      }
    );

    new route53.ARecord(this, "CloudFrontARecord", {
      zone: hostedZone,
      recordName: `${hostedZone.zoneName}`,
      target: route53.RecordTarget.fromAlias(
        new route53Target.CloudFrontTarget(distribution)
      ),
    });
    new route53.AaaaRecord(this, "CloudFrontAaaaRecord", {
      zone: hostedZone,
      recordName: `${hostedZone.zoneName}`,
      target: route53.RecordTarget.fromAlias(
        new route53Target.CloudFrontTarget(distribution)
      ),
    });
  }
}
